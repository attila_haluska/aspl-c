//
// Created by Attila Haluska on 8/2/18.
//

#include "debug/debug.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "compiler/lexer/token.h"
#include "compiler/parser/ast/ast_node.h"
#include "memory_management/memory.h"
#include "data_structures/bytecode_array.h"
#include "vm/vm_opcode.h"
#include "vm/value.h"

static char *debug_token_type(const token_t *token) {
    switch (token->type) {
        case TOKEN_LEFT_PAREN:
            return "TOKEN_LEFT_PAREN";
        case TOKEN_RIGHT_PAREN:
            return "TOKEN_RIGHT_PAREN";
        case TOKEN_LEFT_BRACE:
            return "TOKEN_LEFT_BRACE";
        case TOKEN_RIGHT_BRACE:
            return "TOKEN_RIGHT_BRACE";
        case TOKEN_COMMA:
            return "TOKEN_COMMA";
        case TOKEN_DOT:
            return "TOKEN_DOT";
        case TOKEN_MINUS:
            return "TOKEN_MINUS";
        case TOKEN_MINUS_MINUS:
            return "TOKEN_MINUS_MINUS";
        case TOKEN_PLUS:
            return "TOKEN_PLUS";
        case TOKEN_PLUS_PLUS:
            return "TOKEN_PLUS_PLUS";
        case TOKEN_SEMICOLON:
            return "TOKEN_SEMICOLON";
        case TOKEN_SLASH:
            return "TOKEN_SLASH";
        case TOKEN_STAR:
            return "TOKEN_STAR";
        case TOKEN_QUESTION:
            return "TOKEN_QUESTION";
        case TOKEN_COLON:
            return "TOKEN_COLON";
        case TOKEN_BANG:
            return "TOKEN_BANG";
        case TOKEN_BANG_EQUAL:
            return "TOKEN_BANG_EQUAL";
        case TOKEN_EQUAL:
            return "TOKEN_EQUAL";
        case TOKEN_EQUAL_EQUAL:
            return "TOKEN_EQUAL_EQUAL";
        case TOKEN_GREATER:
            return "TOKEN_GREATER";
        case TOKEN_GREATER_EQUAL:
            return "TOKEN_GREATER_EQUAL";
        case TOKEN_LESS:
            return "TOKEN_LESS";
        case TOKEN_LESS_EQUAL:
            return "TOKEN_LESS_EQUAL";
        case TOKEN_IDENTIFIER:
            return "TOKEN_IDENTIFIER";
        case TOKEN_STRING:
            return "TOKEN_STRING";
        case TOKEN_NUMBER:
            return "TOKEN_NUMBER";
        case TOKEN_AND_AND:
            return "TOKEN_AND_AND";
        case TOKEN_CLASS:
            return "TOKEN_CLASS";
        case TOKEN_ELSE:
            return "TOKEN_ELSE";
        case TOKEN_FALSE:
            return "TOKEN_FALSE";
        case TOKEN_FUNC:
            return "TOKEN_FUNC";
        case TOKEN_FOR:
            return "TOKEN_FOR";
        case TOKEN_IF:
            return "TOKEN_IF";
        case TOKEN_NIL:
            return "TOKEN_NIL";
        case TOKEN_PIPE_PIPE:
            return "TOKEN_PIPE_PIPE";
        case TOKEN_PRINT:
            return "TOKEN_PRINT";
        case TOKEN_RETURN:
            return "TOKEN_RETURN";
        case TOKEN_SUPER:
            return "TOKEN_SUPER";
        case TOKEN_THIS:
            return "TOKEN_THIS";
        case TOKEN_TRUE:
            return "TOKEN_TRUE";
        case TOKEN_VAR:
            return "TOKEN_VAR";
        case TOKEN_WHILE:
            return "TOKEN_WHILE";
        case TOKEN_ERROR:
            return "TOKEN_ERROR";
        case TOKEN_EOF:
            return "TOKEN_EOF";
    }

    return "TOKEN_UNKNOWN";
}

void debug_token(token_t *token) {
    char *lexeme = NEW_WITH_SIZE(char, token->length + 1);
    memcpy(lexeme, token->start, token->length);
    lexeme[token->length] = '\0';
    fprintf(stdout, "Token{%s, '%s'}\n", debug_token_type(token), lexeme);
    DELETE_TYPE(char, lexeme);
}

static int depth = 0;
static char *indent = "  ";

static void begin_scope() {
    depth++;
}

static void end_scope() {
    depth--;
}

static void print_with_indent(const char *what, int extra_args, ...) {
    for (int i = 0; i < depth; i++) {
        fprintf(stdout, "%s", indent);
    }

    fprintf(stdout, "%s", what);

    va_list args;
    va_start(args, extra_args);

    for (int i = 0; i < extra_args; i++) {
        const char *arg = va_arg(args, const char*);
        fprintf(stdout, "%s", arg);
    }

    va_end(args);

    fprintf(stdout, "\n");
}

static char *integer_to_string(int64_t number) {
    int length = snprintf(NULL, 0, "%ld", number);
    char *str = NEW_WITH_SIZE(char, length + 1);
    snprintf(str, length + 1, "%ld", number);
    return str;
}

static char *double_to_string(long double number) {
    int length = snprintf(NULL, 0, "%Lf", number);
    char *str = NEW_WITH_SIZE(char, length + 1);
    snprintf(str, length + 1, "%Lf", number);
    return str;
}

static void debug_integer_node(ast_node_t *node) {
    print_with_indent("IntegerExpressionNode {", 0);
    begin_scope();
    char *str = integer_to_string(node->as.integer);
    print_with_indent(str, 0);
    DELETE_TYPE(char, str);
    end_scope();
    print_with_indent("}", 0);
}

static void debug_double_node(ast_node_t *node) {
    print_with_indent("DoubleExpressionNode {", 0);
    begin_scope();
    char *str = double_to_string(node->as.double_precision);
    print_with_indent(str, 0);
    DELETE_TYPE(char, str);
    end_scope();
    print_with_indent("}", 0);
}

static void debug_binary_infix_node(ast_node_t *node) {
    print_with_indent("BinaryInfixOperatorExpressionNode {", 0);
    begin_scope();
    debug_ast_node(node->as.binary_infix.left);
    char *str = token_get_lexeme(&node->token);
    print_with_indent("Operator ", 1, str);
    DELETE_TYPE(char, str);
    debug_ast_node(node->as.binary_infix.right);
    end_scope();
    print_with_indent("}", 0);
}

static void debug_unary_prefix_node(ast_node_t *node) {
    print_with_indent("UnaryPrefixOperatorExpressionNode {", 0);
    begin_scope();
    char *str = token_get_lexeme(&node->token);
    print_with_indent("Operator ", 1, str);
    DELETE_TYPE(char, str);
    debug_ast_node(node->as.unary_prefix.right);
    end_scope();
    print_with_indent("}", 0);
}

static void debug_boolean_node(ast_node_t *node) {
    print_with_indent("BooleanExpressionNode {", 0);
    begin_scope();
    print_with_indent(node->as.boolean ? "true" : "false", 0);
    end_scope();
    print_with_indent("}", 0);
}

static void debug_nil_node(ast_node_t *node) {
    UNUSED(node);
    print_with_indent("NilExpressionNode {", 0);
    begin_scope();
    print_with_indent("nil", 0);
    end_scope();
    print_with_indent("}", 0);
}

static void debug_string_node(ast_node_t *node) {
    print_with_indent("StringExpressionNode {", 0);
    begin_scope();
    print_with_indent(node->as.string.start, 0);
    end_scope();
    print_with_indent("}", 0);
}

void debug_ast_node(ast_node_t *node) {
    switch (node->type) {
        case NODE_INTEGER:
            debug_integer_node(node);
            break;
        case NODE_DOUBLE:
            debug_double_node(node);
            break;
        case NODE_BINARY_INFIX_OPERATOR:
            debug_binary_infix_node(node);
            break;
        case NODE_UNARY_PREFIX_OPERATOR:
            debug_unary_prefix_node(node);
            break;
        case NODE_BOOLEAN:
            debug_boolean_node(node);
            break;
        case NODE_NIL:
            debug_nil_node(node);
            break;
        case NODE_STRING:
            debug_string_node(node);
            break;
    }
}

static int simple_instruction(const char *name, int offset) {
    printf("%s\n", name);
    return offset + 1;
}

static int constant_instruction(const char *name, bytecode_array_t *chunk, int offset) {
    uint8_t constant = chunk->code.values[offset + 1];
    printf("%-16s %14d   '", name, constant);
    value_print(&chunk->constants.values[constant]);
    printf("'\n");
    return offset + 2;
}

int debug_disassemble_instruction(bytecode_array_t *array, int offset) {
    printf("%08d ", offset);

    if (offset > 0 && array->lines.values[offset] == array->lines.values[offset - 1]) {
        printf("      | ");
    } else {
        printf("%7ld ", array->lines.values[offset]);
    }

    uint8_t instruction = array->code.values[offset];

    switch (instruction) {
        case OP_NOP:
            return simple_instruction("OP_NOP", offset);
        case OP_CONSTANT:
            return constant_instruction("OP_CONSTANT", array, offset);
        case OP_ADD:
            return simple_instruction("OP_ADD", offset);
        case OP_SUBTRACT:
            return simple_instruction("OP_SUBTRACT", offset);
        case OP_MULTIPLY:
            return simple_instruction("OP_MULTIPLY", offset);
        case OP_DIVIDE:
            return simple_instruction("OP_DIVIDE", offset);
        case OP_NEGATE:
            return simple_instruction("OP_NEGATE", offset);
        case OP_RETURN:
            return simple_instruction("OP_RETURN", offset);
        case OP_NIL:
            return simple_instruction("OP_NIL", offset);
        case OP_TRUE:
            return simple_instruction("OP_TRUE", offset);
        case OP_FALSE:
            return simple_instruction("OP_FALSE", offset);
        case OP_NOT:
            return simple_instruction("OP_NOT", offset);
        case OP_EQUAL:
            return simple_instruction("OP_EQUAL", offset);
        case OP_GREATER:
            return simple_instruction("OP_GREATER", offset);
        case OP_LESS:
            return simple_instruction("OP_LESS", offset);
        default:
            printf("Unknown opcode %d\n", instruction);
            return offset + 1;
    }
}

void debug_disassemble_bytecode_array(bytecode_array_t *array, const char *name) {
    printf("== <%s> bytecode start ==\n", name);
    printf("%8s %7s %-16s %14s %s\n", "[offset]", "[line]", "[opcode]", "[constant pos]", "  [constant value]");

    for (int i = 0; i < array->code.count;) {
        i = debug_disassemble_instruction(array, i);
    }

    printf("== <%s> bytecode end ==\n", name);
}
