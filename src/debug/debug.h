//
// Created by Attila Haluska on 8/2/18.
//

#ifndef ASPL_C_DEBUG_H
#define ASPL_C_DEBUG_H

#include "forward_declarations.h"

extern void debug_token(token_t *token);

extern void debug_ast_node(ast_node_t *node);

extern void debug_disassemble_bytecode_array(bytecode_array_t *array, const char *name);

extern int debug_disassemble_instruction(bytecode_array_t *array, int offset);

#endif //ASPL_C_DEBUG_H
