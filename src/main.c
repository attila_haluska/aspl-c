//
// Created by Attila Haluska on 8/1/18.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>

#include "memory_management/memory.h"
#include "vm/vm.h"
#include "common.h"

#define MAX_LINE_LENGTH (1024)

static void signal_handler(int signal) {
    UNUSED(signal);
    fprintf(stdout, "Bye\n");
    exit(EXIT_SUCCESS);
}

static size_t file_size(FILE *file) {
    fseek(file, 0L, SEEK_END);
    size_t fileSize = (size_t) ftell(file);
    rewind(file);
    return fileSize;
}

static char *read_file(const char *file_path) {
    FILE *file = fopen(file_path, "rb");

    if (file == NULL) {
        fprintf(stderr, "Could not read file \"%s\"\n", file_path);
        exit(CANNOT_READ_FILE);
    }

    size_t size = file_size(file);

    char *buffer = NEW_WITH_SIZE(char, size + 1);

    if (buffer == NULL) {
        fprintf(stderr, "Could not read file \"%s\"\n", file_path);
        exit(CANNOT_READ_FILE);
    }

    size_t bytes_read = fread(buffer, sizeof(char), size, file);

    if (bytes_read != size) {
        fprintf(stderr, "Could not read file \"%s\"\n", file_path);
        exit(CANNOT_READ_FILE);
    }

    buffer[bytes_read] = '\0';

    fclose(file);

    return buffer;
}

static void run_file(const char *file_path) {
    char *source = read_file(file_path);

    vm_t vm;
    vm_init(&vm);
    vm_interpret_result_t result = vm_interpret(&vm, source);
    vm_free(&vm);

    DELETE_TYPE(char, source);

    if (result == VM_INTERPRET_COMPILE_ERROR) {
        exit(COMPILE_ERROR);
    } else if (result == VM_INTERPRET_RUNTIME_ERROR) {
        exit(RUNTIME_ERROR);
    }
}

static void run_repl() {
    vm_t vm;

    vm_init(&vm);

    char line[MAX_LINE_LENGTH];
    memset(line, 0, MAX_LINE_LENGTH);

    fprintf(stdout, "\nASPL REPL\n\nType 'Ctrl+C', 'quit' or 'q' to exit.\n\naspl> ");

    while (true) {
        if (fgets(line, MAX_LINE_LENGTH, stdin) == NULL) {
            fprintf(stdout, "\n");
            break;
        }

        if (memcmp(line, "quit", 4) != 0 && memcmp(line, "q", 1) != 0) {
            vm_interpret(&vm, line);
            fprintf(stdout, "aspl> ");
        } else {
            fprintf(stdout, "Bye\n");
            break;
        }
    }

    vm_free(&vm);
}

int main(int argc, const char *argv[]) {
    signal(SIGINT, &signal_handler);
    signal(SIGTERM, &signal_handler);

    if (argc > 2) {
        fprintf(stderr, "Usage: aspl [script]\n");
        exit(INVALID_ARGUMENTS);
    } else if (argc == 2) {
        run_file(argv[1]);
    } else {
        run_repl();
    }

    return EXIT_SUCCESS;
}
