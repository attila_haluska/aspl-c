//
// Created by Attila Haluska on 8/1/18.
//

#ifndef ASPL_C_LEXER_H
#define ASPL_C_LEXER_H

#include <stdint.h>

#include "forward_declarations.h"

typedef struct lexer_t {
    const char *token_start;
    const char *current_position;
    const char *current_line_start;
    uint64_t line;
} lexer_t;

extern lexer_t lexer_create_lexer(const char *source);

extern token_t lexer_next_token(lexer_t *lexer);

#endif //ASPL_C_LEXER_H
