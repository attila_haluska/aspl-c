//
// Created by Attila Haluska on 8/13/18.
//

#ifndef ASPL_C_TOKEN_H
#define ASPL_C_TOKEN_H

#include <stdint.h>
#include <stdbool.h>

typedef enum token_type_t {
    TOKEN_LEFT_PAREN,
    TOKEN_RIGHT_PAREN,
    TOKEN_LEFT_BRACE,
    TOKEN_RIGHT_BRACE,
    TOKEN_COMMA,
    TOKEN_DOT,
    TOKEN_MINUS,
    TOKEN_MINUS_MINUS,
    TOKEN_PLUS,
    TOKEN_PLUS_PLUS,
    TOKEN_SEMICOLON,
    TOKEN_SLASH,
    TOKEN_STAR,
    TOKEN_QUESTION,
    TOKEN_COLON,
    TOKEN_AND_AND,
    TOKEN_PIPE_PIPE,

    TOKEN_BANG,
    TOKEN_BANG_EQUAL,
    TOKEN_EQUAL,
    TOKEN_EQUAL_EQUAL,
    TOKEN_GREATER,
    TOKEN_GREATER_EQUAL,
    TOKEN_LESS,
    TOKEN_LESS_EQUAL,

    TOKEN_IDENTIFIER,
    TOKEN_STRING,
    TOKEN_NUMBER,

    TOKEN_CLASS,
    TOKEN_ELSE,
    TOKEN_FALSE,
    TOKEN_FUNC,
    TOKEN_FOR,
    TOKEN_IF,
    TOKEN_NIL,
    TOKEN_PRINT,
    TOKEN_RETURN,
    TOKEN_SUPER,
    TOKEN_THIS,
    TOKEN_TRUE,
    TOKEN_VAR,
    TOKEN_WHILE,

    TOKEN_ERROR,
    TOKEN_EOF
} token_type_t;

typedef struct token_t {
    token_type_t type;
    const char *start;
    const char *line_start;
    const char *error_message;
    uint64_t length;
    uint64_t line;
} token_t;

extern token_t token_create_null_token(void);

extern bool token_is_double(const token_t *token);

extern char *token_get_lexeme(const token_t *token);

#endif //ASPL_C_TOKEN_H
