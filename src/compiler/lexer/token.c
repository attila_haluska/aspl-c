//
// Created by Attila Haluska on 8/13/18.
//

#include "compiler/lexer/token.h"

#include <stddef.h>
#include <string.h>

#include "memory_management/memory.h"

token_t token_create_null_token(void) {
    token_t token = {
            .type = -1,
            .error_message = NULL,
            .length = 0,
            .line_start = NULL,
            .start = NULL
    };

    return token;
}

bool token_is_double(const token_t *token) {
    if (token != NULL) {
        for (int i = 0; i < token->length; i++) {
            char c = *(token->start + i);

            if (c == '.') {
                return true;
            }
        }
    }

    return false;
}

char *token_get_lexeme(const token_t *token) {
    char *str = NEW_WITH_SIZE(char, token->length + 1);
    strncpy(str, token->start, token->length);
    str[token->length] = '\0';
    return str;
}
