//
// Created by Attila Haluska on 8/1/18.
//

#include "lexer.h"

#include <string.h>

#include "compiler/lexer/token.h"
#include "memory_management/memory.h"

typedef struct keyword_t {
    const char *name;
    size_t length;
    token_type_t type;
} Keyword;

static const Keyword keywords[] = {
        {"class",  5, TOKEN_CLASS},
        {"else",   4, TOKEN_ELSE},
        {"false",  5, TOKEN_FALSE},
        {"for",    3, TOKEN_FOR},
        {"func",   4, TOKEN_FUNC},
        {"if",     2, TOKEN_IF},
        {"nil",    3, TOKEN_NIL},
        {"print",  5, TOKEN_PRINT},
        {"return", 6, TOKEN_RETURN},
        {"super",  5, TOKEN_SUPER},
        {"this",   4, TOKEN_THIS},
        {"true",   4, TOKEN_TRUE},
        {"var",    3, TOKEN_VAR},
        {"while",  5, TOKEN_WHILE},
        {NULL,     0, TOKEN_EOF}
};

static token_t create_token(lexer_t *lexer, token_type_t type) {
    token_t token = {
            .type = type,
            .start = lexer->token_start,
            .length = lexer->current_position - lexer->token_start,
            .line = lexer->line,
            .line_start = lexer->current_line_start,
            .error_message = NULL
    };

    return token;
}

static token_t create_error_token(lexer_t *lexer, const char *message) {
    token_t token = {
            .type = TOKEN_ERROR,
            .start = lexer->token_start,
            .length = lexer->current_position - lexer->token_start,
            .line = lexer->line,
            .line_start = lexer->current_line_start,
            .error_message = message
    };

    return token;
}

static bool is_at_end(lexer_t *lexer) {
    return *lexer->current_position == '\0';
}

static char peek(lexer_t *lexer) {
    return *lexer->current_position;
}

static char peek_next(lexer_t *lexer) {
    if (is_at_end(lexer)) {
        return '\0';
    }

    return *(lexer->current_position + 1);
}

static char advance(lexer_t *lexer) {
    lexer->current_position++;
    return *(lexer->current_position - 1);
}

static bool match(lexer_t *lexer, char c) {
    if (is_at_end(lexer)) {
        return false;
    }

    if (*lexer->current_position != c) {
        return false;
    }

    lexer->current_position++;

    return true;
}

static bool is_digit(char c) {
    return c >= '0' && c <= '9';
}

static bool is_alpha(char c) {
    return (c >= 'a' && c <= 'z') ||
           (c >= 'A' && c <= 'Z') ||
           c == '_';
}

static bool is_alphanumeric(char c) {
    return is_alpha(c) || is_digit(c);
}

static bool is_whitespace(lexer_t *lexer) {
    char c = peek(lexer);
    return c == ' ' || c == '\r' || c == '\t' || c == '\n';
}

static bool is_comment(lexer_t *lexer) {
    char current = peek(lexer);
    char next = peek_next(lexer);
    return current == '/' && (next == '/' || next == '*');
}

static token_t scan_string(lexer_t *lexer) {
    while (peek(lexer) != '"' && !is_at_end(lexer)) {
        char c = advance(lexer);

        if (c == '\n') {
            lexer->line++;
            lexer->current_line_start = lexer->current_position;
        }
    }

    if (is_at_end(lexer)) {
        return create_error_token(lexer, "Unterminated string");
    }

    advance(lexer);

    return create_token(lexer, TOKEN_STRING);
}

static token_t scan_number(lexer_t *lexer) {
    while (is_digit(peek(lexer))) {
        advance(lexer);
    }

    if (peek(lexer) == '.' && is_digit(peek_next(lexer))) {
        advance(lexer);

        while (is_digit(peek(lexer))) {
            advance(lexer);
        }
    }

    return create_token(lexer, TOKEN_NUMBER);
}

static token_t scan_identifier(lexer_t *lexer) {
    while (is_alphanumeric(peek(lexer))) {
        advance(lexer);
    }

    token_type_t type = TOKEN_IDENTIFIER;

    size_t length = lexer->current_position - lexer->token_start;

    for (const Keyword *keyword = keywords; keyword->name != NULL; keyword++) {
        if (length == keyword->length && memcmp(lexer->token_start, keyword->name, length) == 0) {
            type = keyword->type;
            break;
        }
    }

    return create_token(lexer, type);
}

static token_t skip_comment(lexer_t *lexer) {
    if (peek(lexer) == '/' && peek_next(lexer) == '/') {
        while (peek(lexer) != '\n' && !is_at_end(lexer)) {
            advance(lexer);
        }

        if (peek(lexer) == '\n') {
            lexer->line++;
            advance(lexer);
        }
    } else if (peek(lexer) == '/' && peek_next(lexer) == '*') {
        bool finished = false;

        while (!finished) {
            char c = advance(lexer);

            if (is_at_end(lexer)) {
                return create_error_token(lexer, "Unterminated comment");
            }

            if (c == '\n') {
                lexer->line++;
                lexer->current_line_start = lexer->current_position;
            }

            if (peek(lexer) == '*' && peek_next(lexer) == '/') {
                advance(lexer);
                advance(lexer);
                finished = true;
            }
        }
    }

    return token_create_null_token();
}

static void skip_whitespace(lexer_t *lexer) {
    for (;;) {
        char c = peek(lexer);

        switch (c) {
            case ' ':
            case '\r':
            case '\t':
                advance(lexer);
                break;
            case '\n':
                advance(lexer);
                lexer->line++;
                lexer->current_line_start = lexer->current_position;
                break;
            default:
                return;
        }
    }
}

lexer_t lexer_create_lexer(const char *source) {
    lexer_t lexer = {
            .token_start = source,
            .current_position = source,
            .current_line_start = source,
            .line = 1,
    };

    return lexer;
}

token_t lexer_next_token(lexer_t *lexer) {
    bool comment;
    bool whitespace;

    do {
        skip_whitespace(lexer);
        token_t error = skip_comment(lexer);

        if (error.type != -1) {
            return error;
        }

        comment = is_comment(lexer);
        whitespace = is_whitespace(lexer);
    } while (comment || whitespace);

    lexer->token_start = lexer->current_position;

    if (is_at_end(lexer)) {
        return create_token(lexer, TOKEN_EOF);
    }

    char c = advance(lexer);

    switch (c) {
        case '(':
            return create_token(lexer, TOKEN_LEFT_PAREN);
        case ')':
            return create_token(lexer, TOKEN_RIGHT_PAREN);
        case '{':
            return create_token(lexer, TOKEN_LEFT_BRACE);
        case '}':
            return create_token(lexer, TOKEN_RIGHT_BRACE);
        case ';':
            return create_token(lexer, TOKEN_SEMICOLON);
        case ',':
            return create_token(lexer, TOKEN_COMMA);
        case '.':
            return create_token(lexer, TOKEN_DOT);
        case '-':
            if (match(lexer, '-')) {
                return create_token(lexer, TOKEN_MINUS_MINUS);
            }

            return create_token(lexer, TOKEN_MINUS);
        case '+':
            if (match(lexer, '+')) {
                return create_token(lexer, TOKEN_PLUS_PLUS);
            }

            return create_token(lexer, TOKEN_PLUS);
        case '/': {
            return create_token(lexer, TOKEN_SLASH);
        }
        case '*':
            return create_token(lexer, TOKEN_STAR);
        case '?':
            return create_token(lexer, TOKEN_QUESTION);
        case ':':
            return create_token(lexer, TOKEN_COLON);
        case '!':
            if (match(lexer, '=')) {
                return create_token(lexer, TOKEN_BANG_EQUAL);
            }

            return create_token(lexer, TOKEN_BANG);
        case '=':
            if (match(lexer, '=')) {
                return create_token(lexer, TOKEN_EQUAL_EQUAL);
            }

            return create_token(lexer, TOKEN_EQUAL);
        case '<':
            if (match(lexer, '=')) {
                return create_token(lexer, TOKEN_LESS_EQUAL);
            }

            return create_token(lexer, TOKEN_LESS);
        case '>':
            if (match(lexer, '=')) {
                return create_token(lexer, TOKEN_GREATER_EQUAL);
            }

            return create_token(lexer, TOKEN_GREATER);
        case '&':
            if (match(lexer, '&')) {
                return create_token(lexer, TOKEN_AND_AND);
            }
        case '|':
            if (match(lexer, '|')) {
                return create_token(lexer, TOKEN_PIPE_PIPE);
            }
        case '"':
            return scan_string(lexer);
        default:
            if (is_digit(c)) {
                return scan_number(lexer);
            } else if (is_alpha(c)) {
                return scan_identifier(lexer);
            }

            break;
    }

    return create_error_token(lexer, "Unexpected character");
}
