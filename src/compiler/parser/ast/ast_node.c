//
// Created by Attila Haluska on 8/13/18.
//

#include "compiler/parser/ast/ast_node.h"

#include <stdlib.h>

#include "compiler/lexer/token.h"
#include "memory_management/memory.h"
#include "ast_node.h"

ast_node_t *ast_node_make_integer(token_t token) {
    ast_node_t *node = NEW(ast_node_t);

    node->type = NODE_INTEGER;
    node->token = token;
    node->as.integer = strtoll(node->token.start, NULL, 10);

    return node;
}

ast_node_t *ast_node_make_double(token_t token) {
    ast_node_t *node = NEW(ast_node_t);

    char *end;

    node->type = NODE_DOUBLE;
    node->token = token;
    node->as.double_precision = strtold(node->token.start, &end);

    return node;
}

ast_node_t *ast_node_make_binary_infix_expression(token_t token, ast_node_t *left, ast_node_t *right) {
    ast_node_t *node = NEW(ast_node_t);

    node->type = NODE_BINARY_INFIX_OPERATOR;
    node->token = token;
    node->as.binary_infix.left = left;
    node->as.binary_infix.right = right;

    return node;
}

ast_node_t *ast_node_make_unary_prefix_expression(token_t token, ast_node_t *right) {
    ast_node_t *node = NEW(ast_node_t);

    node->type = NODE_UNARY_PREFIX_OPERATOR;
    node->token = token;
    node->as.unary_prefix.right = right;

    return node;
}

ast_node_t *ast_node_make_boolean(token_t token) {
    ast_node_t *node = NEW(ast_node_t);

    node->type = NODE_BOOLEAN;
    node->token = token;
    node->as.boolean = token.type == TOKEN_TRUE ? true : false;

    return node;
}

ast_node_t *ast_node_make_nil(token_t token) {
    ast_node_t *node = NEW(ast_node_t);

    node->type = NODE_NIL;
    node->token = token;

    return node;
}

ast_node_t *ast_node_make_string(token_t token) {
    ast_node_t *node = NEW(ast_node_t);

    node->type = NODE_STRING;
    node->token = token;
    node->as.string.length = token.length;
    node->as.string.start = token.start;

    return node;
}

static void ast_node_free_integer_node(ast_node_t *node) {
    DELETE_TYPE(ast_node_t, node);
}

static void ast_node_free_double_node(ast_node_t *node) {
    DELETE_TYPE(ast_node_t, node);
}

static void ast_node_free_binary_infix_node(ast_node_t *node) {
    ast_node_free(node->as.binary_infix.left);
    ast_node_free(node->as.binary_infix.right);
    DELETE_TYPE(ast_node_t, node);
}

static void ast_node_free_unary_prefix_node(ast_node_t *node) {
    ast_node_free(node->as.unary_prefix.right);
    DELETE_TYPE(ast_node_t, node);
}

static void ast_node_free_boolean_node(ast_node_t *node) {
    DELETE_TYPE(ast_node_t, node);
}

static void ast_node_free_nil_node(ast_node_t *node) {
    DELETE_TYPE(ast_node_t, node);
}

static void ast_node_free_string_node(ast_node_t *node) {
    DELETE_TYPE(ast_node_t, node);
}

void ast_node_free(ast_node_t *node) {
    switch (node->type) {
        case NODE_INTEGER:
            ast_node_free_integer_node(node);
            break;
        case NODE_DOUBLE:
            ast_node_free_double_node(node);
            break;
        case NODE_BINARY_INFIX_OPERATOR:
            ast_node_free_binary_infix_node(node);
            break;
        case NODE_UNARY_PREFIX_OPERATOR:
            ast_node_free_unary_prefix_node(node);
            break;
        case NODE_BOOLEAN:
            ast_node_free_boolean_node(node);
            break;
        case NODE_NIL:
            ast_node_free_nil_node(node);
            break;
        case NODE_STRING:
            ast_node_free_string_node(node);
            break;
    }
}
