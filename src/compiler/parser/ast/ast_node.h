//
// Created by Attila Haluska on 8/13/18.
//

#ifndef ASPL_C_AST_NODE_H
#define ASPL_C_AST_NODE_H

#include <stdint.h>
#include <stdbool.h>

#include "forward_declarations.h"
#include "compiler/lexer/token.h"

typedef enum ast_node_type_t {
    NODE_INTEGER,
    NODE_DOUBLE,
    NODE_BINARY_INFIX_OPERATOR,
    NODE_UNARY_PREFIX_OPERATOR,
    NODE_BOOLEAN,
    NODE_NIL,
    NODE_STRING,
} ast_node_type_t;

typedef struct ast_node_t {
    ast_node_type_t type;
    token_t token;
    union {
        struct {
            ast_node_t *left;
            ast_node_t *right;
        } binary_infix;
        struct {
            ast_node_t *right;
        } unary_prefix;
        int64_t integer;
        long double double_precision;
        bool boolean;
        struct {
            int length;
            const char *start;
        } string;
    } as;
} ast_node_t;

extern ast_node_t *ast_node_make_integer(token_t token);

extern ast_node_t *ast_node_make_double(token_t token);

extern ast_node_t *ast_node_make_binary_infix_expression(token_t token, ast_node_t *left, ast_node_t *right);

extern ast_node_t *ast_node_make_unary_prefix_expression(token_t token, ast_node_t *right);

extern ast_node_t *ast_node_make_boolean(token_t token);

extern ast_node_t *ast_node_make_nil(token_t token);

extern ast_node_t *ast_node_make_string(token_t token);

extern void ast_node_free(ast_node_t *node);

#endif //ASPL_C_AST_NODE_H
