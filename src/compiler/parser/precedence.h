//
// Created by Attila Haluska on 8/14/18.
//

#ifndef ASPL_C_PRECEDENCE_H
#define ASPL_C_PRECEDENCE_H

typedef enum precedence_t {
    PREC_NONE = 0,
    PREC_COMMA,         // ,
    PREC_ASSIGNMENT,    // =
    PREC_TERNARY,       // ?:
    PREC_OR,            // or
    PREC_AND,           // and
    PREC_EQUALITY,      // == !=
    PREC_COMPARISON,    // < > <= >=
    PREC_TERM,          // + -
    PREC_FACTOR,        // * /
    PREC_EXPONENT,      // ^
    PREC_UNARY_PREFIX,  // ! - +
    PREC_UNARY_POSTFIX, // ! - +
    PREC_CALL,          // . () []
    PREC_PRIMARY
} precedence_t;

#endif //ASPL_C_PRECEDENCE_H
