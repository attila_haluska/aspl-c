//
// Created by Attila Haluska on 8/13/18.
//

#include "compiler/parser/parser.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "compiler/parser/ast/ast_node.h"
#include "compiler/parser/precedence.h"
#include "compiler/lexer/lexer.h"
#include "compiler/lexer/token.h"
#include "memory_management/memory.h"

typedef struct parser_t {
    token_t previous_token;
    token_t current_token;
    bool had_error;
    bool panic_mode;
} parser_t;

typedef struct parsing_context_t {
    lexer_t lexer;
    parser_t parser;
} parsing_context_t;

typedef ast_node_t *(*nud_parse_function_t)(parsing_context_t *context);

typedef ast_node_t *(*led_parse_function_t)(parsing_context_t *context, ast_node_t *left);

typedef struct parse_rule_t {
    nud_parse_function_t nud;
    led_parse_function_t led;
    precedence_t precedence;
} parse_rule_t;

static parse_rule_t *get_parse_rule(token_t token);

static precedence_t get_precedence(token_t token);

static ast_node_t *parse_expression(parsing_context_t *context);

static parser_t create_parser(void) {
    parser_t parser = {
            .previous_token = token_create_null_token(),
            .current_token = token_create_null_token(),
            .had_error = false,
            .panic_mode = false,
    };

    return parser;
}

static parsing_context_t create_parsing_context(const char *source) {
    parsing_context_t context = {
            .lexer = lexer_create_lexer(source),
            .parser = create_parser(),
    };

    return context;
}

inline static const token_t get_current_token(parsing_context_t *context) {
    return context->parser.current_token;
}

inline static const token_t get_previous_token(parsing_context_t *context) {
    return context->parser.previous_token;
}

static void error_at_token(parsing_context_t *context, const token_t token, const char *message) {
    if (context->parser.panic_mode) {
        return;
    }

    context->parser.panic_mode = true;
    context->parser.had_error = true;

    if (token.type == TOKEN_ERROR) {
        fprintf(stderr, "[line %lu] Syntax error", token.line);
    } else {
        fprintf(stderr, "[line %lu] Parse error", token.line);
    }

    if (token.type == TOKEN_EOF) {
        fprintf(stderr, " at the end of file");
    } else {
        fprintf(stderr, " at '%.*s'", (int) token.length, token.start);
    }

    fprintf(stderr, ": %s\n", message);
}

static void error_at_previous_token(parsing_context_t *context, const char *message) {
    error_at_token(context, get_previous_token(context), message);
}

static void error_at_current_token(parsing_context_t *context, const char *message) {
    error_at_token(context, get_current_token(context), message);
}

static void advance_to_next_token(parsing_context_t *context) {
    context->parser.previous_token = context->parser.current_token;

    for (;;) {
        context->parser.current_token = lexer_next_token(&context->lexer);

        if (get_current_token(context).type != TOKEN_ERROR) {
            break;
        }

        error_at_current_token(context, get_current_token(context).error_message);
    }
}

static bool check_current_token_type(parsing_context_t *context, token_type_t type) {
    return get_current_token(context).type == type;
}

static void consume_current_token_if_type(parsing_context_t *context, token_type_t type, const char *message) {
    if (check_current_token_type(context, type)) {
        advance_to_next_token(context);
        return;
    }

    error_at_current_token(context, message);
}

static ast_node_t *parse_precedence(parsing_context_t *context, precedence_t precedence) {
    advance_to_next_token(context);
    nud_parse_function_t prefix_rule = get_parse_rule(get_previous_token(context))->nud;

    if (prefix_rule == NULL) {
        error_at_previous_token(context, "Expect expression");
    }

    ast_node_t *node = prefix_rule(context);

    while (precedence < get_precedence(get_current_token(context))) {
        advance_to_next_token(context);
        led_parse_function_t infix_rule = get_parse_rule(get_previous_token(context))->led;
        node = infix_rule(context, node);
    }

    return node;
}

static ast_node_t *parse_expression(parsing_context_t *context) {
    return parse_precedence(context, PREC_ASSIGNMENT);
}

static ast_node_t *parse_number_literal_expression(parsing_context_t *context) {
    const token_t token = get_previous_token(context);

    if (token_is_double(&token)) {
        return ast_node_make_double(token);
    } else {
        return ast_node_make_integer(token);
    }
}

static ast_node_t *parse_nil_literal_expression(parsing_context_t *context) {
    const token_t token = get_previous_token(context);
    return ast_node_make_nil(token);
}

static ast_node_t *parse_true_literal_expression(parsing_context_t *context) {
    const token_t token = get_previous_token(context);
    return ast_node_make_boolean(token);
}

static ast_node_t *parse_false_literal_expression(parsing_context_t *context) {
    const token_t token = get_previous_token(context);
    return ast_node_make_boolean(token);
}

static ast_node_t *parse_string_literal_expression(parsing_context_t *context) {
    const token_t token = get_previous_token(context);
    return ast_node_make_string(token);
}

static ast_node_t *parse_binary_operator_expression(parsing_context_t *context, ast_node_t *left) {
    const token_t previous = get_previous_token(context);
    parse_rule_t *rule = get_parse_rule(previous);
    ast_node_t *right = parse_precedence(context, rule->precedence);
    return ast_node_make_binary_infix_expression(previous, left, right);
}

static ast_node_t *parse_unary_prefix_operator_expression(parsing_context_t *context) {
    const token_t previous = get_previous_token(context);
    ast_node_t *right = parse_precedence(context, PREC_UNARY_PREFIX - 1);
    return ast_node_make_unary_prefix_expression(previous, right);
}

static ast_node_t *parse_grouping_expression(parsing_context_t *context) {
    ast_node_t *node = parse_expression(context);
    consume_current_token_if_type(context, TOKEN_RIGHT_PAREN, "Expect ')' after expression");
    return node;
}

static parse_rule_t parse_rules[] = {
        {parse_grouping_expression,              NULL,                             PREC_CALL},    // TOKEN_LEFT_PAREN,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_RIGHT_PAREN,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_LEFT_BRACE,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_RIGHT_BRACE,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_COMMA,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_DOT,
        {parse_unary_prefix_operator_expression, parse_binary_operator_expression, PREC_TERM},    // TOKEN_MINUS,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_MINUS_MINUS,
        {NULL,                                   parse_binary_operator_expression, PREC_TERM},    // TOKEN_PLUS,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_PLUS_PLUS,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_SEMICOLON,
        {NULL,                                   parse_binary_operator_expression, PREC_FACTOR},  // TOKEN_SLASH,
        {NULL,                                   parse_binary_operator_expression, PREC_FACTOR},  // TOKEN_STAR,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_QUESTION,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_COLON,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_AND_AND,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_PIPE_PIPE,

        {parse_unary_prefix_operator_expression, NULL,                             PREC_NONE},          // TOKEN_BANG,
        {NULL,                                   parse_binary_operator_expression, PREC_EQUALITY},      // TOKEN_BANG_EQUAL,
        {NULL,                                   NULL,                             PREC_NONE},          // TOKEN_EQUAL,
        {NULL,                                   parse_binary_operator_expression, PREC_EQUALITY},      // TOKEN_EQUAL_EQUAL,
        {NULL,                                   parse_binary_operator_expression, PREC_COMPARISON},    // TOKEN_GREATER,
        {NULL,                                   parse_binary_operator_expression, PREC_COMPARISON},    // TOKEN_GREATER_EQUAL,
        {NULL,                                   parse_binary_operator_expression, PREC_COMPARISON},    // TOKEN_LESS,
        {NULL,                                   parse_binary_operator_expression, PREC_COMPARISON},    // TOKEN_LESS_EQUAL,

        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_IDENTIFIER,
        {parse_string_literal_expression,        NULL,                             PREC_NONE},    // TOKEN_STRING,
        {parse_number_literal_expression,        NULL,                             PREC_NONE},    // TOKEN_NUMBER,

        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_CLASS,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_ELSE,
        {parse_false_literal_expression,         NULL,                             PREC_NONE},    // TOKEN_FALSE,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_FUNC,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_FOR,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_IF,
        {parse_nil_literal_expression,           NULL,                             PREC_NONE},    // TOKEN_NIL,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_PRINT,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_RETURN,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_SUPER,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_THIS,
        {parse_true_literal_expression,          NULL,                             PREC_NONE},    // TOKEN_TRUE,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_VAR,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_WHILE,

        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_ERROR,
        {NULL,                                   NULL,                             PREC_NONE},    // TOKEN_EOF
};

static parse_rule_t *get_parse_rule(const token_t token) {
    return &parse_rules[token.type];
}

static precedence_t get_precedence(const token_t token) {
    parse_rule_t *rule = get_parse_rule(token);
    return rule->precedence;
}

bool parser_parse(const char *source, ast_node_t **root_node_inout) {
    if (root_node_inout != NULL) {
        parsing_context_t context = create_parsing_context(source);
        advance_to_next_token(&context);
        *root_node_inout = parse_expression(&context);
        return !context.parser.had_error;
    }

    return true;
}
