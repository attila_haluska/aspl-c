//
// Created by Attila Haluska on 8/13/18.
//

#ifndef ASPL_C_PARSER_H
#define ASPL_C_PARSER_H

#include "forward_declarations.h"

extern bool parser_parse(const char *source, ast_node_t **root_node_inout);

#endif //ASPL_C_PARSER_H
