//
// Created by Attila Haluska on 9/8/18.
//

#ifndef ASPL_C_COMPILER_H
#define ASPL_C_COMPILER_H

#include "forward_declarations.h"

extern bool compiler_compile_source(vm_t *vm, const char *source, bytecode_array_t **bytecode_inout);

#endif //ASPL_C_COMPILER_H
