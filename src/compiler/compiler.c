//
// Created by Attila Haluska on 9/8/18.
//

#include "compiler/compiler.h"

#include <stdio.h>

#include "compiler/lexer/token.h"
#include "compiler/parser/ast/ast_node.h"
#include "compiler/parser/parser.h"
#include "data_structures/bytecode_array.h"
#include "vm/value.h"
#include "vm/vm_opcode.h"
#include "vm/vm.h"

#define CONSTANTS_MAX   UINT8_MAX

typedef struct compilation_context_t {
    vm_t *vm;
    bytecode_array_t *current_bytecode;
    ast_node_t *current_node;
    bool had_error;
} compilation_context_t;

static void compile_ast_node(compilation_context_t *context);

static void error(compilation_context_t *context, const char *message) {
    context->had_error = true;

    fprintf(stderr, "[line %lu] Compile error", context->current_node->token.line);

    if (context->current_node->token.type == TOKEN_EOF) {
        fprintf(stderr, " at the end of file");
    } else {
        fprintf(stderr, " at '%.*s'", (int) context->current_node->token.length, context->current_node->token.start);
    }

    fprintf(stderr, ": %s\n", message);
}

static void emit_byte(compilation_context_t *context, uint8_t byte) {
    bytecode_array_add_bytecode(context->current_bytecode, byte, context->current_node->token.line);
}

static void emit_bytes(compilation_context_t *context, uint8_t byte1, uint8_t byte2) {
    emit_byte(context, byte1);
    emit_byte(context, byte2);
}

static uint8_t make_constant(compilation_context_t *context, const value_t value) {
    uint16_t constant = bytecode_array_add_constant(context->current_bytecode, value);

    if (constant > CONSTANTS_MAX) {
        error(context, "Too many constants in one bytecode");
    }

    return (uint8_t) constant;
}

static void emit_constant(compilation_context_t *context, const value_t value) {
    emit_bytes(context, OP_CONSTANT, make_constant(context, value));
}

static void compile_integer_node(compilation_context_t *context) {
    value_t value = value_new_integer_value(context->current_node->as.integer);
    emit_constant(context, value);
}

static void compile_double_node(compilation_context_t *context) {
    value_t value = value_new_double_value(context->current_node->as.double_precision);
    emit_constant(context, value);
}

static void compile_binary_infix_node(compilation_context_t *context) {
    ast_node_t *current_node = context->current_node;

    context->current_node = current_node->as.binary_infix.left;
    compile_ast_node(context);

    context->current_node = current_node->as.binary_infix.right;
    compile_ast_node(context);

    context->current_node = current_node;

    switch (current_node->token.type) {
        case TOKEN_PLUS:
            emit_byte(context, OP_ADD);
            break;
        case TOKEN_MINUS:
            emit_byte(context, OP_SUBTRACT);
            break;
        case TOKEN_STAR:
            emit_byte(context, OP_MULTIPLY);
            break;
        case TOKEN_SLASH:
            emit_byte(context, OP_DIVIDE);
            break;
        case TOKEN_BANG_EQUAL:
            emit_bytes(context, OP_EQUAL, OP_NOT);
            break;
        case TOKEN_EQUAL_EQUAL:
            emit_byte(context, OP_EQUAL);
            break;
        case TOKEN_GREATER_EQUAL:
            emit_bytes(context, OP_LESS, OP_NOT);
            break;
        case TOKEN_GREATER:
            emit_byte(context, OP_GREATER);
            break;
        case TOKEN_LESS_EQUAL:
            emit_bytes(context, OP_GREATER, OP_NOT);
            break;
        case TOKEN_LESS:
            emit_byte(context, OP_LESS);
            break;
        default:
            break;
    }
}

static void compile_unary_prefix_node(compilation_context_t *context) {
    ast_node_t *current_node = context->current_node;

    context->current_node = current_node->as.unary_prefix.right;
    compile_ast_node(context);

    context->current_node = current_node;

    switch (context->current_node->token.type) {
        case TOKEN_MINUS:
            emit_byte(context, OP_NEGATE);
            break;
        case TOKEN_BANG:
            emit_byte(context, OP_NOT);
            break;
        default:
            break;
    }
}

static void compile_boolean_node(compilation_context_t *context) {
    context->current_node->as.boolean ? emit_byte(context, OP_TRUE) : emit_byte(context, OP_FALSE);
}

static void compile_nil_node(compilation_context_t *context) {
    emit_byte(context, OP_NIL);
}

static void compile_string_node(compilation_context_t *context) {
    bool is_interned = false;

    managed_string_t *string = managed_object_new_string(&context->vm->string_pool,
                                                         context->current_node->as.string.start + 1,
                                                         context->current_node->as.string.length - 2,
                                                         true,
                                                         &is_interned);
    managed_object_t *object = (managed_object_t *) string;

    if (!is_interned) {
        vm_add_managed_object(context->vm, object);
    }

    emit_constant(context, value_new_object_value(object));
}

static void compile_ast_node(compilation_context_t *context) {
    switch (context->current_node->type) {
        case NODE_INTEGER:
            compile_integer_node(context);
            break;
        case NODE_DOUBLE:
            compile_double_node(context);
            break;
        case NODE_BINARY_INFIX_OPERATOR:
            compile_binary_infix_node(context);
            break;
        case NODE_UNARY_PREFIX_OPERATOR:
            compile_unary_prefix_node(context);
            break;
        case NODE_BOOLEAN:
            compile_boolean_node(context);
            break;
        case NODE_NIL:
            compile_nil_node(context);
            break;
        case NODE_STRING:
            compile_string_node(context);
            break;
    }
}

bool compiler_compile_source(vm_t *vm, const char *source, bytecode_array_t **bytecode_inout) {
    if (bytecode_inout != NULL) {
        ast_node_t *root_node;

        if (!parser_parse(source, &root_node)) {
            return false;
        }

        compilation_context_t context = {
                .vm = vm,
                .current_bytecode = *bytecode_inout,
                .current_node = root_node,
                .had_error = false,
        };

        compile_ast_node(&context);
        emit_byte(&context, OP_RETURN);
        ast_node_free(root_node);

        return !context.had_error;
    }

    return true;
}
