//
// Created by attila on 5/12/19.
//

#ifndef ASPL_C_HASH_MAP_H
#define ASPL_C_HASH_MAP_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "forward_declarations.h"
#include "vm/value.h"

typedef struct hash_map_entry_t {
    managed_string_t *key;
    value_t value;
} hash_map_entry_t;

typedef struct hash_map_t {
    size_t count;
    size_t capacity;
    hash_map_entry_t *entries;
} hash_map_t;

extern void hash_map_init(hash_map_t *map);

extern void hash_map_free(hash_map_t *map);

extern bool hash_map_set_value_for_key(hash_map_t *map, managed_string_t *key, value_t value);

extern bool hash_map_get_value_for_key(hash_map_t *map, managed_string_t *key, value_t *value);

extern bool hash_map_remove_key(hash_map_t *map, managed_string_t *key);

extern void hash_map_add_all(hash_map_t *from, hash_map_t *to);

extern managed_string_t *hash_map_find_key(hash_map_t *map, const char *chars, uint64_t length, uint32_t hash);

#endif //ASPL_C_HASH_MAP_H
