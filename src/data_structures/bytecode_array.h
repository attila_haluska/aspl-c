//
// Created by Attila Haluska on 9/7/18.
//

#ifndef ASPL_C_BYTECODE_ARRAY_H
#define ASPL_C_BYTECODE_ARRAY_H

#include <stddef.h>
#include <stdint.h>

#include "forward_declarations.h"
#include "data_structures/uint8_array.h"
#include "data_structures/uint64_array.h"
#include "data_structures/value_array.h"

typedef struct bytecode_array_t {
    uint8_array_t code;
    uint64_array_t lines;
    value_array_t constants;
} bytecode_array_t;

extern void bytecode_array_init(bytecode_array_t *array);

extern void bytecode_array_free(bytecode_array_t *array);

extern void bytecode_array_add_bytecode(bytecode_array_t *array, uint8_t byte_code, uint64_t line);

extern uint16_t bytecode_array_add_constant(bytecode_array_t *array, value_t value);

#endif //ASPL_C_BYTECODE_ARRAY_H
