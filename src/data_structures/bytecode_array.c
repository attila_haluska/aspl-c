//
// Created by Attila Haluska on 9/7/18.
//

#include "data_structures/bytecode_array.h"

#include "vm/value.h"

extern void bytecode_array_init(bytecode_array_t *array) {
    uint8_array_init(&array->code);
    uint64_array_init(&array->lines);
    value_array_init(&array->constants);
}

extern void bytecode_array_free(bytecode_array_t *array) {
    if (array->code.count > 0) {
        uint8_array_free(&array->code);
        uint64_array_free(&array->lines);
    }

    value_array_free(&array->constants);

    bytecode_array_init(array);
}

extern void bytecode_array_add_bytecode(bytecode_array_t *array, uint8_t byte_code, uint64_t line) {
    uint8_array_add_value(&array->code, byte_code);
    uint64_array_add_value(&array->lines, line);
}

extern uint16_t bytecode_array_add_constant(bytecode_array_t *array, value_t value) {
    value_array_add_value(&array->constants, value);
    return (uint16_t) (array->constants.count - 1);
}
