//
// Created by attila on 5/12/19.
//

#include "data_structures/hash_map.h"

#include <string.h>

#include "memory_management/managed_object.h"
#include "memory_management/memory.h"

#define TABLE_MAX_LOAD 0.75

static hash_map_entry_t *hash_map_find_entry(hash_map_entry_t *entries,
                                             size_t capacity,
                                             managed_string_t *key) {
    uint64_t index = key->hash & (capacity - 1);
    hash_map_entry_t *tombstone = NULL;

    for (;;) {
        hash_map_entry_t *entry = &entries[index];

        if (entry->key == NULL) {
            if (IS_NIL(entry->value)) {
                return tombstone != NULL ? tombstone : entry;
            } else {
                if (tombstone == NULL) {
                    tombstone = entry;
                }
            }
        } else if (entry->key == key) {
            return entry;
        }

        index = (index + 1) & (capacity - 1);
    }
}

static void hash_map_resize(hash_map_t *map, size_t capacity) {
    hash_map_entry_t *entries = GROW_DYNAMIC_ARRAY(hash_map_entry_t, NULL, 0, capacity);

    for (int i = 0; i < capacity; i++) {
        entries[i].key = NULL;
        entries[i].value = value_new_nil_value();
    }

    map->count = 0;

    for (uint64_t i = 0; i < map->capacity; i++) {
        hash_map_entry_t *entry = &map->entries[i];

        if (entry->key == NULL) {
            continue;
        }

        hash_map_entry_t *destination = hash_map_find_entry(entries, capacity, entry->key);

        destination->key = entry->key;
        destination->value = entry->value;

        map->count++;
    }

    DELETE_DYNAMIC_ARRAY(hash_map_entry_t, map->entries, map->capacity);

    map->entries = entries;
    map->capacity = capacity;
}

void hash_map_init(hash_map_t *map) {
    map->count = 0;
    map->capacity = 0;
    map->entries = NULL;
}

void hash_map_free(hash_map_t *map) {
    if (map != NULL) {
        if (map->count > 0) {
            DELETE_DYNAMIC_ARRAY(hash_map_entry_t, map->entries, map->capacity);
        }

        hash_map_init(map);
    }
}

bool hash_map_set_value_for_key(hash_map_t *map, managed_string_t *key, value_t value) {
    if (map->count + 1 > map->capacity * TABLE_MAX_LOAD) {
        size_t capacity = memory_grow_capacity(map->capacity);
        hash_map_resize(map, capacity);
    }

    hash_map_entry_t *entry = hash_map_find_entry(map->entries, map->capacity, key);

    bool is_new_key = entry->key == NULL;

    if (is_new_key && IS_NIL(entry->value)) {
        map->count++;
    }

    entry->key = key;
    entry->value = value;

    return is_new_key;
}

bool hash_map_get_value_for_key(hash_map_t *map, managed_string_t *key, value_t *value) {
    if (map->entries == NULL) {
        return false;
    }

    hash_map_entry_t *entry = hash_map_find_entry(map->entries, map->capacity, key);

    if (entry->key == NULL) {
        return false;
    }

    *value = entry->value;

    return true;
}

bool hash_map_remove_key(hash_map_t *map, managed_string_t *key) {
    if (map->count == 0) {
        return false;
    }

    hash_map_entry_t *entry = hash_map_find_entry(map->entries, map->capacity, key);

    if (entry->key == NULL) {
        return false;
    }

    // create a tombstone
    entry->key = NULL;
    entry->value = value_new_boolean_value(true);

    return true;
}

void hash_map_add_all(hash_map_t *from, hash_map_t *to) {
    for (uint64_t i = 0; i <= from->capacity; i++) {
        hash_map_entry_t *entry = &from->entries[i];

        if (entry->key != NULL) {
            hash_map_set_value_for_key(to, entry->key, entry->value);
        }
    }
}

managed_string_t *hash_map_find_key(hash_map_t *map, const char *chars, uint64_t length, uint32_t hash) {
    if (map->entries == NULL) {
        return NULL;
    }

    uint64_t index = hash & (map->capacity - 1);

    for (;;) {
        hash_map_entry_t *entry = &map->entries[index];

        if (entry->key == NULL) {
            if (IS_NIL(entry->value)) {
                return NULL;
            }
        } else if (entry->key->length == length &&
                   entry->key->hash == hash &&
                   memcmp(entry->key->characters, chars, length) == 0) {
            return entry->key;
        }

        index = (index + 1) & (map->capacity - 1);
    }
}
