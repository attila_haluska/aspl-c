//
// Created by Attila Haluska on 9/7/18.
//

#ifndef ASPL_C_GENERIC_ARRAY_H
#define ASPL_C_GENERIC_ARRAY_H

#include <stddef.h>
#include <stdint.h>

#include "common.h"
#include "memory_management/memory.h"

#define GA_MAKE_FUNCTION(return_type, prefix, name, ...) static return_type CONCATENATE(prefix, name) (__VA_ARGS__)
#define GA_CALL_FUNCTION(prefix, name, ...) CONCATENATE(prefix, name)(__VA_ARGS__)
#define GA_ARRAY_TYPE(array_name) CONCATENATE(array_name, _t)

#endif //ASPL_C_GENERIC_ARRAY_H

// Important: this section is NOT protected by the header guard, because we want these functions to be included several times with different signatures.
// This makes it possible to "generate" different functions for different types during compilation

typedef struct GA_ARRAY_TYPE(__GENERIC_ARRAY_STRUCT_NAME__) {
    size_t capacity;
    size_t count;
    __GENERIC_ARRAY_TYPE__ *values;
} __GENERIC_ARRAY_NAME__;

GA_MAKE_FUNCTION(void, __GENERIC_ARRAY_STRUCT_NAME__, _init, __GENERIC_ARRAY_NAME__ * array) {
    array->capacity = 0;
    array->count = 0;
    array->values = NULL;
}

GA_MAKE_FUNCTION(void, __GENERIC_ARRAY_STRUCT_NAME__, _free, __GENERIC_ARRAY_NAME__ * array) {
    if (array != NULL) {
        if (array->count > 0) {
            DELETE_DYNAMIC_ARRAY(__GENERIC_ARRAY_TYPE__, array->values, array->capacity);
        }

        GA_CALL_FUNCTION(__GENERIC_ARRAY_STRUCT_NAME__, _init, array);
    }
}

GA_MAKE_FUNCTION(void,
                 __GENERIC_ARRAY_STRUCT_NAME__,
                 _free_with_deleter,
                 __GENERIC_ARRAY_NAME__ * array,
                 void (*deleter)(__GENERIC_ARRAY_TYPE__ value)) {
    if (array != NULL) {
        if (array->count > 0) {
            if (deleter != NULL) {
                for (int i = 0; i < array->count; i++) {
                    deleter(*(array->values + i));
                }
            }

            DELETE_DYNAMIC_ARRAY(__GENERIC_ARRAY_TYPE__, array->values, array->capacity);
        }

        GA_CALL_FUNCTION(__GENERIC_ARRAY_STRUCT_NAME__, _init, array);
    }
}

GA_MAKE_FUNCTION(void,
                 __GENERIC_ARRAY_STRUCT_NAME__,
                 _add_value,
                 __GENERIC_ARRAY_NAME__ * array,
                 __GENERIC_ARRAY_TYPE__
                         value) {
    if (array->capacity < array->count + 1) {
        size_t oldCapacity = array->capacity;
        array->capacity = memory_grow_capacity(oldCapacity);
        array->values = GROW_DYNAMIC_ARRAY(__GENERIC_ARRAY_TYPE__, array->values, array->count, array->capacity);
    }

    array->values[array->count++] = value;
}

GA_MAKE_FUNCTION(__GENERIC_ARRAY_TYPE__,
                 __GENERIC_ARRAY_STRUCT_NAME__,
                 _get_value,
                 __GENERIC_ARRAY_NAME__ * array,
                 size_t index) {
    if (array == NULL || index > array->count - 1) {
        return (__GENERIC_ARRAY_TYPE__) 0;
    }

    return *(array->values + index);
}

GA_MAKE_FUNCTION(size_t, __GENERIC_ARRAY_STRUCT_NAME__, _count, __GENERIC_ARRAY_NAME__ * array) {
    return array->count;
}

GA_MAKE_FUNCTION(void,
                 __GENERIC_ARRAY_STRUCT_NAME__,
                 _set,
                 __GENERIC_ARRAY_NAME__ * array,
                 size_t index,
                 __GENERIC_ARRAY_TYPE__
                         value) {
    if (array == NULL || index > array->count - 1) {
        return;
    }

    array->values[index] = value;
}
