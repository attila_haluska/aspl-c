//
// Created by Attila Haluska on 9/7/18.
//

#ifndef ASPL_C_UINT64_ARRAY_H
#define ASPL_C_UINT64_ARRAY_H

#if defined __GENERIC_ARRAY_TYPE__
#undef __GENERIC_ARRAY_TYPE__
#endif

#if defined __GENERIC_ARRAY_STRUCT_NAME__
#undef __GENERIC_ARRAY_STRUCT_NAME__
#endif

#if defined __GENERIC_ARRAY_NAME__
#undef __GENERIC_ARRAY_NAME__
#endif

#define __GENERIC_ARRAY_TYPE__ uint64_t
#define __GENERIC_ARRAY_NAME__ uint64_array_t
#define __GENERIC_ARRAY_STRUCT_NAME__ uint64_array

#include "data_structures/generic_array.h"

#endif //ASPL_C_UINT64_ARRAY_H
