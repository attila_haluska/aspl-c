//
// Created by attila on 5/11/19.
//

#include "data_structures/value_array.h"

#include "memory_management/memory.h"
#include "vm/value.h"

void value_array_init(value_array_t *array) {
    array->capacity = 0;
    array->count = 0;
    array->values = NULL;
}

void value_array_free(value_array_t *array) {
    if (array != NULL) {
        if ((array)->count > 0) {
            DELETE_DYNAMIC_ARRAY(value_array_t, array->values, array->capacity);
        }

        value_array_init(array);
    }
}

void value_array_add_value(value_array_t *array, value_t value) {
    if (array->capacity < array->count + 1) {
        size_t oldCapacity = array->capacity;
        array->capacity = memory_grow_capacity(oldCapacity);
        array->values = GROW_DYNAMIC_ARRAY(value_t, array->values, array->count, array->capacity);
    }

    array->values[array->count++] = value;
}

value_t value_array_get_value(value_array_t *array, size_t index) {
    if (array == NULL || index > array->count - 1) {
        return value_new_invalid_value();
    }

    return *(array->values + index);
}

size_t value_array_count(value_array_t *array) {
    return array->count;
}

void value_array_set_value(value_array_t *array, size_t index, value_t value) {
    if (array == NULL || index > array->count - 1) {
        return;
    }

    array->values[index] = value;
}
