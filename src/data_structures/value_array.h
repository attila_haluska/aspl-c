//
// Created by attila on 5/11/19.
//

#ifndef ASPL_C_VALUE_ARRAY_H
#define ASPL_C_VALUE_ARRAY_H

#include <stddef.h>

#include "forward_declarations.h"

typedef struct value_array_t {
    size_t capacity;
    size_t count;
    value_t *values;
} value_array_t;

extern void value_array_init(value_array_t *array);

extern void value_array_free(value_array_t *array);

extern void value_array_add_value(value_array_t *array, value_t value);

extern value_t value_array_get_value(value_array_t *array, size_t index);

extern size_t value_array_count(value_array_t *array);

extern void value_array_set_value(value_array_t *array, size_t index, value_t value);

#endif //ASPL_C_VALUE_ARRAY_H
