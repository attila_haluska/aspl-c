//
// Created by Attila Haluska on 9/8/18.
//

#include "vm/vm_functions.h"

#include <stddef.h>
#include <stdarg.h>
#include <stdio.h>
#include <memory.h>

#include "vm/value.h"
#include "vm/vm.h"

uint8_t vm_read_byte(vm_t *vm) {
    return *vm->ip++;
}

value_t vm_read_constant(vm_t *vm) {
    return vm->code.constants.values[vm_read_byte(vm)];
}

void vm_push(vm_t *vm, value_t value) {
    *vm->stack_top = value;
    vm->stack_top++;
}

value_t vm_pop(vm_t *vm) {
    *vm->stack_top = value_new_invalid_value();
    vm->stack_top--;
    value_t value = *vm->stack_top;
    return value;
}

value_t vm_peek(vm_t *vm, uint8_t distance) {
    return vm->stack_top[-1 - distance];
}

void vm_reset_stack(vm_t *vm) {
    vm->stack_top = vm->stack;
}

void vm_runtime_error(vm_t *vm, const char *format, ...) {
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    fputs("\n", stderr);

    size_t instruction = vm->ip - vm->code.code.values - 1;
    fprintf(stderr, "[line %lu] in script\n", vm->code.lines.values[instruction]);

    vm_reset_stack(vm);
}

void vm_concatenate_string(vm_t *vm) {
    managed_string_t *second = AS_STRING(vm_pop(vm));
    managed_string_t *first = AS_STRING(vm_pop(vm));

    uint64_t length = first->length + second->length;
    char *buffer = NEW_WITH_SIZE(char, length + 1);

    memcpy(buffer, first->characters, first->length);
    memcpy(buffer + first->length, second->characters, second->length);

    buffer[length] = '\0';

    bool is_interned = false;

    managed_string_t *string = managed_object_new_string(&vm->string_pool, buffer, length, false, &is_interned);

    if (!is_interned) {
        vm_add_managed_object(vm, (managed_object_t *) string);
    } else {
        DELETE_DYNAMIC_ARRAY(char, buffer, length + 1);
    }

    vm_push(vm, value_new_object_value((managed_object_t *) string));
}

void vm_binary_operation(vm_t *vm,
                         vm_binary_operation_t operation,
                         bool *should_return,
                         vm_interpret_result_t *result) {
    if (IS_NUMBER(vm_peek(vm, 0)) && IS_NUMBER(vm_peek(vm, 1))) {
        value_t b = vm_pop(vm);
        value_t a = vm_pop(vm);

        if (IS_DOUBLE(a) && !(IS_DOUBLE(b))) {
            b = value_new_double_value((long double) AS_INTEGER(b));
        }

        if (IS_DOUBLE(b) && !(IS_DOUBLE(a))) {
            a = value_new_double_value((long double) AS_INTEGER(a));
        }

        value_t new_value = value_new_invalid_value();

        switch (operation) {
            case VM_BIN_OP_ADD:
                if (IS_DOUBLE(a) && IS_DOUBLE(b)) {
                    new_value = value_new_double_value(AS_DOUBLE(a) + AS_DOUBLE(b));
                } else {
                    new_value = value_new_integer_value(AS_INTEGER(a) + AS_INTEGER(b));
                }
                break;
            case VM_BIN_OP_SUBTRACT:
                if (IS_DOUBLE(a) && IS_DOUBLE(b)) {
                    new_value = value_new_double_value(AS_DOUBLE(a) - AS_DOUBLE(b));
                } else {
                    new_value = value_new_integer_value(AS_INTEGER(a) - AS_INTEGER(b));
                }
                break;
            case VM_BIN_OP_MULTIPLY:
                if (IS_DOUBLE(a) && IS_DOUBLE(b)) {
                    new_value = value_new_double_value(AS_DOUBLE(a) * AS_DOUBLE(b));
                } else {
                    new_value = value_new_integer_value(AS_INTEGER(a) * AS_INTEGER(b));
                }
                break;
            case VM_BIN_OP_DIVIDE:
                if (IS_DOUBLE(a) && IS_DOUBLE(b)) {
                    new_value = value_new_double_value(AS_DOUBLE(a) / AS_DOUBLE(b));
                } else {
                    new_value = value_new_integer_value(AS_INTEGER(a) / AS_INTEGER(b));
                }
                break;
            default:
                break;
        }

        vm_push(vm, new_value);

        return;
    } else if (IS_STRING(vm_peek(vm, 0)) && IS_STRING(vm_peek(vm, 1)) && operation == VM_BIN_OP_ADD) {
        vm_concatenate_string(vm);
        return;
    }

    *should_return = true;
    *result = VM_INTERPRET_RUNTIME_ERROR;
    vm_runtime_error(vm, "Operands must be numbers");
}

void vm_binary_comparison(vm_t *vm,
                          vm_binary_comparison_t operation,
                          bool *should_return,
                          vm_interpret_result_t *result) {
    if (IS_NUMBER(vm_peek(vm, 0)) && IS_NUMBER(vm_peek(vm, 1))) {
        value_t b = vm_pop(vm);
        value_t a = vm_pop(vm);

        if (IS_DOUBLE(a) && !(IS_DOUBLE(b))) {
            b = value_new_double_value((long double) AS_INTEGER(b));
        }

        if (IS_DOUBLE(b) && !(IS_DOUBLE(a))) {
            a = value_new_double_value((long double) AS_INTEGER(a));
        }

        value_t new_value = value_new_invalid_value();

        switch (operation) {
            case VM_BIN_COMP_LESS:
                if (IS_DOUBLE(a) && IS_DOUBLE(b)) {
                    new_value = value_new_boolean_value(AS_DOUBLE(a) < AS_DOUBLE(b));
                } else {
                    new_value = value_new_boolean_value(AS_INTEGER(a) < AS_INTEGER(b));
                }
                break;
            case VM_BIN_COMP_GREATER:
                if (IS_DOUBLE(a) && IS_DOUBLE(b)) {
                    new_value = value_new_boolean_value(AS_DOUBLE(a) > AS_DOUBLE(b));
                } else {
                    new_value = value_new_boolean_value(AS_INTEGER(a) > AS_INTEGER(b));
                }
                break;
            default:
                break;
        }

        vm_push(vm, new_value);

        return;
    }

    *should_return = true;
    *result = VM_INTERPRET_RUNTIME_ERROR;
    vm_runtime_error(vm, "Operands must be numbers");
}

bool vm_value_is_false(const value_t value) {
    return IS_NIL(value) || (IS_BOOLEAN(value) && !AS_BOOLEAN(value));
}
