//
// Created by Attila Haluska on 9/8/18.
//

#ifndef ASPL_C_VM_OPCODE_FUNCTIONS_H
#define ASPL_C_VM_OPCODE_FUNCTIONS_H

#include <stdbool.h>

#include "forward_declarations.h"

typedef void (*vm_opcode_function_t)(vm_t *vm, bool *should_return, vm_interpret_result_t *result);

extern const vm_opcode_function_t vm_opcode_functions[];

#endif //ASPL_C_VM_OPCODE_FUNCTIONS_H
