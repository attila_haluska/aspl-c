//
// Created by Attila Haluska on 9/7/18.
//

#ifndef ASPL_C_VALUE_H
#define ASPL_C_VALUE_H

#include <stdint.h>
#include <stdbool.h>

#include "forward_declarations.h"

#define IS_DOUBLE(value)    (value).type == VAL_DOUBLE
#define IS_INTEGER(value)   (value).type == VAL_INTEGER
#define IS_NUMBER(value)    is_number((value))
#define IS_BOOLEAN(value)   (value).type == VAL_BOOLEAN
#define IS_NIL(value)       (value).type == VAL_NIL
#define IS_INVALID(value)   (value).type == VAL_INVALID
#define IS_OBJECT(value)    (value).type == VAL_OBJECT

#define AS_DOUBLE(value)    (value).as.double_value
#define AS_INTEGER(value)   (value).as.integer_value
#define AS_BOOLEAN(value)   (value).as.boolean_value
#define AS_OBJECT(value)    (value).as.object_value

typedef enum value_type_t {
    VAL_INVALID,
    VAL_NIL,
    VAL_DOUBLE,
    VAL_INTEGER,
    VAL_BOOLEAN,
    VAL_OBJECT,
} value_type_t;

typedef struct value_t {
    value_type_t type;
    union {
        long double double_value;
        int64_t integer_value;
        bool boolean_value;
        managed_object_t *object_value;
    } as;
} value_t;

extern value_t value_new_double_value(long double value);

extern value_t value_new_integer_value(int64_t value);

extern value_t value_new_boolean_value(bool value);

extern value_t value_new_nil_value();

extern value_t value_new_invalid_value();

extern value_t value_new_object_value(managed_object_t *value);

extern void value_print(const value_t *value);

extern bool value_equals(const value_t *one, const value_t *other);

static inline bool is_number(const value_t value) {
    return IS_DOUBLE(value) || IS_INTEGER(value);
}

#endif //ASPL_C_VALUE_H
