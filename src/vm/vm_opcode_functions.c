//
// Created by Attila Haluska on 9/8/18.
//

#include "vm/vm_opcode_functions.h"

#include <stdio.h>

#include "vm/value.h"
#include "vm/vm.h"
#include "vm/vm_functions.h"

static void op_nop(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {

}

static void op_constant(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    value_t constant = vm_read_constant(vm);
    vm_push(vm, constant);
}

static void op_add(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    vm_binary_operation(vm, VM_BIN_OP_ADD, should_return, result);
}

static void op_subtract(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    vm_binary_operation(vm, VM_BIN_OP_SUBTRACT, should_return, result);
}

static void op_multiply(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    vm_binary_operation(vm, VM_BIN_OP_MULTIPLY, should_return, result);
}

static void op_divide(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    vm_binary_operation(vm, VM_BIN_OP_DIVIDE, should_return, result);
}

static void op_negate(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    if (!IS_NUMBER(vm_peek(vm, 0))) {
        *should_return = true;
        *result = VM_INTERPRET_RUNTIME_ERROR;
        vm_runtime_error(vm, "Operand must be a number");
        return;
    }

    value_t value = vm_pop(vm);

    if (IS_INTEGER(value)) {
        AS_INTEGER(value) = -AS_INTEGER(value);
    } else if (IS_DOUBLE(value)) {
        AS_DOUBLE(value) = -AS_DOUBLE(value);
    }

    vm_push(vm, value);
}

static void op_return(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    vm_pop(vm);
    *should_return = true;
    *result = VM_INTERPRET_OK;
}

static void op_true(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    value_t new_value = value_new_boolean_value(true);
    vm_push(vm, new_value);
}

static void op_false(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    value_t new_value = value_new_boolean_value(false);
    vm_push(vm, new_value);
}

static void op_nil(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    value_t new_value = value_new_nil_value(vm);
    vm_push(vm, new_value);
}

static void op_not(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    value_t value = vm_pop(vm);
    value = value_new_boolean_value(vm_value_is_false(value));
    vm_push(vm, value);
}

static void op_equal(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    value_t first = vm_pop(vm);
    value_t second = vm_pop(vm);

    first = value_new_boolean_value(value_equals(&first, &second));

    vm_push(vm, first);
}

static void op_less(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    vm_binary_comparison(vm, VM_BIN_COMP_LESS, should_return, result);
}

static void op_greater(vm_t *vm, bool *should_return, vm_interpret_result_t *result) {
    vm_binary_comparison(vm, VM_BIN_COMP_GREATER, should_return, result);
}

const vm_opcode_function_t vm_opcode_functions[] = {
        op_nop,             // OP_NOP
        op_constant,        // OP_CONSTANT,
        op_add,             // OP_ADD,
        op_subtract,        // OP_SUBTRACT,
        op_multiply,        // OP_MULTIPLY,
        op_divide,          // OP_DIVIDE,
        op_negate,          // OP_NEGATE,
        op_return,          // OP_RETURN,
        op_nil,             // OP_NIL,
        op_true,            // OP_TRUE,
        op_false,           // OP_FALSE,
        op_not,             // OP_NOT,
        op_equal,           // OP_EQUAL,
        op_less,            // OP_LESS,
        op_greater,         // OP_GREATER,
};
