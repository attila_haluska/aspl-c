//
// Created by Attila Haluska on 9/7/18.
//

#include "vm/value.h"

#include <stdio.h>
#include <assert.h>
#include <memory.h>

#include "memory_management/memory.h"
#include "memory_management/managed_object.h"

value_t value_new_double_value(long double value) {
    value_t v = {
            .type = VAL_DOUBLE,
            .as.double_value = value,
    };

    return v;
}

value_t value_new_integer_value(int64_t value) {
    value_t v = {
            .type = VAL_INTEGER,
            .as.integer_value = value,
    };

    return v;
}

value_t value_new_boolean_value(bool value) {
    value_t v = {
            .type = VAL_BOOLEAN,
            .as.boolean_value = value,
    };

    return v;
}

value_t value_new_nil_value() {
    value_t v = {
            .type = VAL_NIL,
            .as.integer_value = 0,
    };

    return v;
}

value_t value_new_invalid_value() {
    value_t v = {
            .type = VAL_INVALID,
            .as.integer_value = 0,
    };

    return v;
}

value_t value_new_object_value(managed_object_t *value) {
    value_t v = {
            .type = VAL_OBJECT,
            .as.object_value = value,
    };

    return v;
}

void value_print(const value_t *value) {
    switch (value->type) {
        case VAL_INTEGER:
            fprintf(stdout, "%ld", AS_INTEGER(*value));
            break;
        case VAL_DOUBLE:
            fprintf(stdout, "%Lf", AS_DOUBLE(*value));
            break;
        case VAL_BOOLEAN:
            fprintf(stdout, "%s", AS_BOOLEAN(*value) ? "true" : "false");
            break;
        case VAL_NIL:
            fprintf(stdout, "nil");
            break;
        case VAL_INVALID:
            fprintf(stdout, "invalid (uninitialized)");
            break;
        case VAL_OBJECT:
            managed_object_print(AS_OBJECT(*value));
            break;
        default:
            fprintf(stdout, "unknown");
            break;
    }
}

bool value_equals(const value_t *one, const value_t *other) {
    assert(one != NULL && other != NULL);

    if (one->type != other->type) {
        return false;
    }

    switch (one->type) {
        case VAL_NIL:
            return true;
        case VAL_BOOLEAN:
            return AS_BOOLEAN(*one) == AS_BOOLEAN(*other);
        case VAL_INTEGER:
            return AS_INTEGER(*one) == AS_INTEGER(*other);
        case VAL_DOUBLE:
            return AS_DOUBLE(*one) == AS_DOUBLE(*other);
        case VAL_OBJECT: {
            if (AS_OBJECT(*one)->type != AS_OBJECT(*other)->type) {
                return false;
            }

            switch (AS_OBJECT(*one)->type) {
                case OBJ_STRING:
                    return AS_STRING(*one) == AS_STRING(*other);
            }
        }
        default:
            return false;
    }
}
