//
// Created by Attila Haluska on 9/7/18.
//

#ifndef ASPL_C_VM_OPCODE_H
#define ASPL_C_VM_OPCODE_H

typedef enum opcode_t {
    OP_NOP = 0,
    OP_CONSTANT = 1,
    OP_ADD = 2,
    OP_SUBTRACT = 3,
    OP_MULTIPLY = 4,
    OP_DIVIDE = 5,
    OP_NEGATE = 6,
    OP_RETURN = 7,
    OP_NIL = 8,
    OP_TRUE = 9,
    OP_FALSE = 10,
    OP_NOT = 11,
    OP_EQUAL = 12,
    OP_LESS = 13,
    OP_GREATER = 14,
} opcode_t;

#endif //ASPL_C_VM_OPCODE_H
