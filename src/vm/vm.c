//
// Created by Attila Haluska on 9/8/18.
//

#include "vm/vm.h"

#include <stdbool.h>
#include <stdio.h>

#include "compiler/compiler.h"
#include "vm/value.h"
#include "vm/vm_functions.h"
#include "vm/vm_opcode.h"
#include "vm/vm_opcode_functions.h"
#include "common.h"

#if defined DEBUG_TRACE_EXECUTION

#include "debug/debug.h"

#endif

#if defined DEBUG_TRACE_EXECUTION

static void vm_trace_execution(vm_t *vm) {
    printf("== Stack ==    ");

    for (value_t *slot = vm->stack; slot < vm->stack_top; slot++) {
        printf("[");
        value_print(slot);
        printf("] ");
    }

    printf("\n");

    debug_disassemble_instruction(&vm->code, (int) (vm->ip - vm->code.code.values));
}

#endif

void vm_init(vm_t *vm) {
    for (int i = 0; i < STACK_MAX; i++) {
        vm->stack[i] = value_new_invalid_value();
    }

    vm->ip = NULL;
    vm->managed_objects = NULL;
    hash_map_init(&vm->string_pool);

    vm_reset_stack(vm);
}

void vm_free(vm_t *vm) {
    if (vm != NULL) {
        managed_object_free_object_list(vm->managed_objects);
        hash_map_free(&vm->string_pool);
        vm_init(vm);
    }
}

static vm_interpret_result_t vm_run(vm_t *vm) {
#if defined DEBUG_TRACE_EXECUTION
    printf("== <%s> bytecode start ==\n", "main");
    printf("%8s %7s %-16s %14s %s\n", "[offset]", "[line]", "[opcode]", "[constant pos]", "  [constant value]");
#endif

    for (;;) {
#if defined DEBUG_TRACE_EXECUTION
        vm_trace_execution(vm);
#endif

        opcode_t instruction = (opcode_t) vm_read_byte(vm);
        bool should_return = false;
        vm_interpret_result_t result = VM_INTERPRET_OK;

        vm_opcode_functions[instruction](vm, &should_return, &result);

        if (should_return) {
#if defined DEBUG_TRACE_EXECUTION
            printf("== <%s> bytecode end ==\n", "main");
#endif
            return result;
        }
    }
}

vm_interpret_result_t vm_interpret(vm_t *vm, const char *source) {
    bytecode_array_t bytecode;
    bytecode_array_t *bytecode_pointer = &bytecode;

    bytecode_array_init(bytecode_pointer);
    bool compile_successful = compiler_compile_source(vm, source, &bytecode_pointer);

    vm_interpret_result_t result = VM_INTERPRET_COMPILE_ERROR;

    if (compile_successful) {
        vm->code = bytecode;
        vm->ip = vm->code.code.values;
        result = vm_run(vm);
        vm_reset_stack(vm);
    }

    bytecode_array_free(bytecode_pointer);

    return result;
}

void vm_add_managed_object(vm_t *vm, managed_object_t *object) {
    if (vm != NULL && object != NULL) {
        object->next_object = vm->managed_objects;
        vm->managed_objects = object;
    }
}
