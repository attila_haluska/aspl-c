//
// Created by Attila Haluska on 9/8/18.
//

#ifndef ASPL_C_VM_H
#define ASPL_C_VM_H

#include "data_structures/bytecode_array.h"
#include "vm/value.h"
#include "memory_management/managed_object.h"
#include "data_structures/hash_map.h"

#define STACK_MAX   (256)

typedef enum vm_interpret_result_t {
    VM_INTERPRET_OK = 0,
    VM_INTERPRET_COMPILE_ERROR,
    VM_INTERPRET_RUNTIME_ERROR,
} vm_interpret_result_t;

typedef struct vm_t {
    bytecode_array_t code;
    uint8_t *ip;
    value_t stack[STACK_MAX];
    value_t *stack_top;
    managed_object_t *managed_objects;
    hash_map_t string_pool;
} vm_t;

extern void vm_init(vm_t *vm);

extern void vm_free(vm_t *vm);

extern vm_interpret_result_t vm_interpret(vm_t *vm, const char *source);

extern void vm_add_managed_object(vm_t *vm, managed_object_t *object);

#endif //ASPL_C_VM_H
