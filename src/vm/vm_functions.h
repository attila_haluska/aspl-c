//
// Created by Attila Haluska on 9/8/18.
//

#ifndef ASPL_C_VM_FUNCTIONS_H
#define ASPL_C_VM_FUNCTIONS_H

#include <stdint.h>
#include <stdbool.h>

#include "forward_declarations.h"

typedef enum vm_binary_operation_t {
    VM_BIN_OP_ADD,
    VM_BIN_OP_SUBTRACT,
    VM_BIN_OP_MULTIPLY,
    VM_BIN_OP_DIVIDE,
} vm_binary_operation_t;

typedef enum vm_binary_comparison_t {
    VM_BIN_COMP_LESS,
    VM_BIN_COMP_GREATER,
} vm_binary_comparison_t;

extern uint8_t vm_read_byte(vm_t *vm);

extern value_t vm_read_constant(vm_t *vm);

extern void vm_push(vm_t *vm, value_t value);

extern value_t vm_pop(vm_t *vm);

extern value_t vm_peek(vm_t *vm, uint8_t distance);

extern void vm_reset_stack(vm_t *vm);

extern void vm_runtime_error(vm_t *vm, const char *format, ...);

extern void
vm_binary_operation(vm_t *vm, vm_binary_operation_t operation, bool *should_return, vm_interpret_result_t *result);

extern void
vm_binary_comparison(vm_t *vm, vm_binary_comparison_t operation, bool *should_return, vm_interpret_result_t *result);

extern bool vm_value_is_false(value_t value);

#endif //ASPL_C_VM_FUNCTIONS_H
