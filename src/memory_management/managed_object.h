//
// Created by attila on 5/12/19.
//

#ifndef ASPL_C_MANAGED_OBJECT_H
#define ASPL_C_MANAGED_OBJECT_H

#include <stdbool.h>
#include <stdint.h>

#include "forward_declarations.h"
#include "vm/value.h"

#define OBJECT_TYPE(value)  (AS_OBJECT(value)->type)

#define IS_STRING(value)    is_object_type((value), OBJ_STRING)

#define AS_STRING(value)    ((managed_string_t*)AS_OBJECT(value))
#define AS_CSTRING(value)   (AS_STRING(value)->characters)

typedef enum managed_object_type_t {
    OBJ_STRING,
} managed_object_type_t;

typedef struct managed_object_t {
    managed_object_type_t type;
    managed_object_t *next_object;
} managed_object_t;

typedef struct managed_string_t {
    managed_object_t object;
    bool is_copied;
    uint64_t length;
    char *characters;
    uint32_t hash;
} managed_string_t;

extern managed_string_t *managed_object_new_string(hash_map_t *string_pool,
                                                   const char *string,
                                                   int length,
                                                   bool copy,
                                                   bool *is_interned);

extern managed_string_t *managed_object_copy_string(const char *string, int length);

extern void managed_object_delete_string(managed_string_t *string);

extern void managed_object_print(managed_object_t *object);

extern void managed_object_free(managed_object_t *object);

extern void managed_object_free_object_list(managed_object_t *root_object);

static inline bool is_object_type(const value_t value, managed_object_type_t type) {
    return IS_OBJECT(value) && OBJECT_TYPE(value) == type;
}

#endif //ASPL_C_MANAGED_OBJECT_H
