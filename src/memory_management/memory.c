//
// Created by Attila Haluska on 8/2/18.
//

#include "memory_management/memory.h"

#include <memory.h>
#include <stdlib.h>

#include "common.h"

inline size_t memory_grow_capacity(size_t current_size) {
    return current_size < GROW_CAPACITY_THRESHOLD ? GROW_CAPACITY_THRESHOLD : current_size * GROW_CAPACITY_FACTOR;
}

void *memory_reallocate(void *original_memory_location, size_t current_size, size_t new_size) {
    if (new_size == 0) {
        free(original_memory_location);
        return NULL;
    }

    void *new_memory_location = realloc(original_memory_location, new_size);

    if (new_memory_location == NULL) {
        exit(BAD_ALLOC);
    }

    return new_memory_location;
}
