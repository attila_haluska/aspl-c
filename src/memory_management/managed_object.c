//
// Created by attila on 5/12/19.
//

#include "memory_management/managed_object.h"

#include <memory.h>
#include <stdio.h>

#include "memory_management/memory.h"
#include "data_structures/hash_map.h"

#define NEW_OBJECT(type, objectType)    (type*)new_object(sizeof(type), (objectType))

static uint32_t hash_string(const char *key, uint64_t length) {
    uint32_t hash = 2166136261u;

    for (uint64_t i = 0; i < length; i++) {
        hash ^= (unsigned char) key[i];
        hash *= 16777619;
    }

    return hash;
}

static managed_object_t *new_object(size_t size, managed_object_type_t type) {
    managed_object_t *object = (managed_object_t *) memory_reallocate(NULL, 0, size);
    object->type = type;
    return object;
}

managed_string_t *managed_object_new_string(hash_map_t *string_pool,
                                            const char *string,
                                            int length,
                                            bool copy,
                                            bool *is_interned) {
    uint32_t hash = hash_string(string, length);

    managed_string_t *interned = hash_map_find_key(string_pool, string, length, hash);

    if (interned != NULL) {
        if (is_interned != NULL) {
            *is_interned = true;
        }

        return interned;
    }

    if (is_interned != NULL) {
        *is_interned = false;
    }

    managed_string_t *s = NEW_OBJECT(managed_string_t, OBJ_STRING);

    char *characters = (char *) string;

    if (copy) {
        characters = NEW_WITH_SIZE(char, length + 1);
        memcpy(characters, string, length);
        characters[length] = '\0';
    }

    s->length = length;
    s->characters = characters;
    s->is_copied = copy;
    s->hash = hash;

    hash_map_set_value_for_key(string_pool, s, value_new_nil_value());

    return s;
}

managed_string_t *managed_object_copy_string(const char *string, int length) {
    managed_string_t *s = NEW_OBJECT(managed_string_t, OBJ_STRING);

    char *characters = NEW_WITH_SIZE(char, length + 1);
    memcpy(characters, string, length);
    characters[length] = '\0';

    s->length = length;
    s->characters = characters;
    s->is_copied = true;

    return s;
}

void managed_object_delete_string(managed_string_t *string) {
    if (string->characters != NULL) {
        DELETE_DYNAMIC_ARRAY(char, string->characters, string->length + 1);
    }

    DELETE_TYPE(managed_string_t, string);
}

void managed_object_print(managed_object_t *object) {
    if (object != NULL) {
        switch (object->type) {
            case OBJ_STRING: {
                managed_string_t *string = (managed_string_t *) object;
                fprintf(stdout, "%.*s", (int) string->length, string->characters);
                break;
            }
            default:
                break;
        }
    }
}

void managed_object_free(managed_object_t *object) {
    if (object != NULL) {
        switch (object->type) {
            case OBJ_STRING:
                managed_object_delete_string((managed_string_t *) object);
                break;
            default:
                break;
        }
    }
}

void managed_object_free_object_list(managed_object_t *root_object) {
    managed_object_t *current = root_object;
    managed_object_t *next = NULL;

    while (current != NULL) {
        next = current->next_object;
        managed_object_free(current);
        current = next;
    }
}
