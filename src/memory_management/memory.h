//
// Created by Attila Haluska on 8/2/18.
//

#ifndef ASPL_C_MEMORY_H
#define ASPL_C_MEMORY_H

#include <stddef.h>

#define GROW_CAPACITY_THRESHOLD (8)
#define GROW_CAPACITY_FACTOR    (2)

#define NEW(type)                                               (type*)memory_reallocate(NULL, 0, (size_t)sizeof(type))
#define NEW_WITH_SIZE(type, size)                               (type*)memory_reallocate(NULL, 0, (size_t)((size) * sizeof(type)))
#define DELETE_TYPE(type, pointer)                              memory_reallocate((pointer), sizeof(type), 0)
#define GROW_DYNAMIC_ARRAY(type, pointer, currentSize, newSize) (type*)memory_reallocate((pointer), sizeof(type) * (currentSize), sizeof(type) * (newSize))
#define DELETE_DYNAMIC_ARRAY(type, pointer, capacity)           memory_reallocate((pointer), sizeof(type) * (capacity), 0)

extern size_t memory_grow_capacity(size_t current_size);

extern void *memory_reallocate(void *original_memory_location, size_t current_size, size_t new_size);

#endif //ASPL_C_MEMORY_H
