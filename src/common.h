//
// Created by Attila Haluska on 8/1/18.
//

#ifndef ASPL_C_COMMON_H
#define ASPL_C_COMMON_H

#define CONCATENATE(a, b) a##b

#define BAD_ALLOC (50)
#define CANNOT_READ_FILE (55)
#define INVALID_ARGUMENTS (60)
#define COMPILE_ERROR (65)
#define RUNTIME_ERROR (70)

#define DEBUG_TRACE_EXECUTION

#define UNUSED(value) (void)((value))

#endif //ASPL_C_COMMON_H
