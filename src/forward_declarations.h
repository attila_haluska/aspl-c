//
// Created by Attila Haluska on 8/2/18.
//

#ifndef ASPL_C_FORWARD_DECLARATIONS_H
#define ASPL_C_FORWARD_DECLARATIONS_H

#include <stdbool.h>

// token.h
typedef enum token_type_t token_type_t;
typedef struct token_t token_t;

// lexer.h
typedef struct lexer_t lexer_t;

// ast_node.h
typedef struct ast_node_t ast_node_t;

// precedence.h
typedef enum precedence_t precedence_t;

// uint8_array.h
typedef struct uint8_array_t uint8_array_t;

// uint64_array.h
typedef struct uint64_array_t uint64_array_t;

// bytecode_array.h
typedef struct bytecode_array_t bytecode_array_t;

// value_array.h
typedef struct value_array_t value_array_t;

// vm_opcode.h
typedef enum opcode_t opcode_t;

// value.h
typedef enum value_type_t value_type_t;
typedef struct value_t value_t;

// vm.h
typedef enum vm_interpret_result_t vm_interpret_result_t;
typedef struct vm_t vm_t;

// vm_functions.h
typedef enum vm_binary_operation_t vm_binary_operation_t;
typedef enum vm_binary_comparison_t vm_binary_comparison_t;

// vm_opcode_functions.h
typedef void (*vm_opcode_function_t)(vm_t *vm, bool *shouldReturn, vm_interpret_result_t *result);

// managed_object.h
typedef enum managed_object_type_t managed_object_type_t;
typedef struct managed_object_t managed_object_t;
typedef struct managed_string_t managed_string_t;

// hash_map.h
typedef struct hash_map_entry_t hash_map_entry_t;
typedef struct hash_map_t hash_map_t;

#endif //ASPL_C_FORWARD_DECLARATIONS_H
